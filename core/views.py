from django.views.generic.base import TemplateView
from django.shortcuts import render

class HomePageView(TemplateView):
    template_name = "core/home.html"

    def get(self, request, *args, **kargs):
        return render(request, self.template_name, {'title':'Mi super web Playground'})

class SampleView(TemplateView):
    template_name = "core/sample.html"